#!/bin/bash

# This script updates all art files in this repository using the chesscraft repository.
# First it deletes everything, then it copies from ~/projects/chesscraft
# Must be run from the chesscraft-creative-commons folder.

set -exu

SOURCE=$HOME/projects/chesscraft
FOLDER="chesscraft-creative-commons"

if [ $(basename $PWD) != "$FOLDER" ] ; then
	echo "Run this script from the folder: $FOLDER"
	exit 1
fi

if [ ! -d $SOURCE ] ; then
	echo "Source directory does not exist: $SOURCE"
	exit 1
fi

# Copy Piece PNGs
PIECES_FOLDER=pieces
rm -rf $PIECES_FOLDER
mkdir -p $PIECES_FOLDER
EXCLUDES="--exclude=*.meta --exclude=*.xcf"
PIECE_SOURCE_FOLDERS="$SOURCE/Assets/Resources/Pieces $SOURCE/Assets/Sprites/holiday"
for piece_source_folder in $PIECE_SOURCE_FOLDERS ; do 
	rsync -vr $piece_source_folder/ $PIECES_FOLDER/ $EXCLUDES
done

# Themes
rm -rf themes
rsync -vr $SOURCE/Assets/Sprites/themes . $EXCLUDES

# Copy Board PNGs
rm -rf board
rsync -vr $SOURCE/Assets/Sprites/board . $EXCLUDES

# Generate preview PNG
if [ ! -d venv ] ; then
	python3 -m venv venv
	venv/bin/pip3 install -r requirements.txt
fi

echo "Generating Previews"

venv/bin/python3 scripts/generate-previews.py

echo "Done."
